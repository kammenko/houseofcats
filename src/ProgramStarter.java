import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProgramStarter {

//    There are some people and cats in a house. You are given the number of legs they have all together.
//    Your task is to return an array containing every possible number of people that could be in the house sorted in ascending order.
//    It's guaranteed that each person has 2 legs and each cat has 4 legs.

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Input the number of legs n∈[0,50)");
        String inputs = input.next();
        while (true) {

            while (!inputs.matches("^([0-4]?[0-9]?)$")) {

                System.out.println("Please enter the number between 0 and 50");
                inputs = input.next();
            }

            int n = Integer.parseInt(inputs);
            if (n % 2 != 0) {

                System.out.println("There is no possible solution for the odd number of legs! Every person in the house has a full bill of health. Same goes for cats.");
                inputs = "-1";
            } else {

                houseOfCats(n);
                /* For the recursive test uncomment the row below and comment out the row above */
//              System.out.println("Possible number of people in the House is "+ houseOfCatsRecursive(new ArrayList<>(), n));
                break;
            }


        }
    }

    public static void houseOfCats(int legs) {

        if (legs == 0) {

            System.out.println("There are no people nor cats in the house!");
            return;
        }

        List<Integer> possibNumberOfPeople = new ArrayList<>();
        List<Integer> theRemainingLegsAreCats = new ArrayList<>();
        int catCountHelper = legs;
        while (legs >= 0) {

            if (legs % 2 == 0) {

                possibNumberOfPeople.add(0, legs / 2);
                theRemainingLegsAreCats.add(0, (catCountHelper - possibNumberOfPeople.get(0) * 2) / 4);
            }
            legs -= 4;
        }

        System.out.println("Possible number of people in the House is " + possibNumberOfPeople);
        System.out.println("And they can play with " + theRemainingLegsAreCats + " cats!");
    }

    public static List<Integer> houseOfCatsRecursive(List<Integer> possibleNumberOfPeople, int legs) {

        if (legs == 0) {

            possibleNumberOfPeople.add(0, 0);
            return possibleNumberOfPeople;
        } else {

            possibleNumberOfPeople.add(0, legs / 2);
            return houseOfCatsRecursive(possibleNumberOfPeople, legs - 4);
        }
    }
}
